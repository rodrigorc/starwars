import React, { useContext } from 'react';
import CharacterInput from './CharacterInput';
import { AutocompleteContext } from '../contexts/AutocompleteProvider';
import CharacterList from './CharacterList';
import { useHttp } from '../hooks/https';

const CharacterAutocomplete = props => {
    const { updateItemsList, suggestions, inputValue, setInputValue, itemsList, setSuggestions } = useContext(AutocompleteContext);
    const [isLoading, fetchedData] = useHttp('https://swapi.co/api/people', []);

    const filterItems = value => {
        let filteredSuggestions = [];
        if (value.length > 0 && itemsList) {
            filteredSuggestions = itemsList.filter(item => {
                return item.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1;
            });
        }
        setInputValue(value);
        setSuggestions(filteredSuggestions);
    }

    if (fetchedData) {
        updateItemsList(fetchedData.results);
    }

    return (
        <div className="main__container">
            <div className="field__container">
                <CharacterInput type="text" onChangeHandler={filterItems} value={inputValue} />
                <CharacterList items={suggestions} onClickHandler={setInputValue} />
            </div>
        </div>
    );
}

export default CharacterAutocomplete;