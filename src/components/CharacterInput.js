import React from 'react';

const CharacterInput = props => {
    return (
        <input type={props.type}
            onChange={(e) => props.onChangeHandler(e.target.value)}
            className={"field__input-" + props.type}
            value={props.value}
        ></input>
    );
}

export default CharacterInput;