import React from 'react';

const CharacterList = props => {
    return (
        <div className={'dropdown__list ' + (props.items ? 'is-active' : '')}>
            {
                props.items ? props.items.map(item => {
                    return (
                        <div key={item.name} style={{ color: 'white' }} onClick={() => { props.onClickHandler(item.name) }} data-name={item.name}>
                            {item.name}
                        </div>
                    );
                }) : ''
            }
        </div>
    );
}

export default CharacterList;