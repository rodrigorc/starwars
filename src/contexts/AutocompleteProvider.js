import React, { useState } from 'react';

export const AutocompleteContext = React.createContext();

const AutocompleteProvider = props => {
    const [itemsList, setItemsList] = useState(null);
    const [suggestions, setSuggestions] = useState(null);
    const [inputValue, setInputValue] = useState('');

    const updateItemsList = items => {
        setItemsList(items);
    }

    return (
        <AutocompleteContext.Provider value={{ itemsList, updateItemsList, suggestions, setSuggestions, inputValue, setInputValue }}>
            {props.children}
        </AutocompleteContext.Provider>
    );
}

export default AutocompleteProvider;