import React from 'react';
import './App.css';
import CharacterAutocomplete from './components/CharacterAutocomplete';
import AutocompleteProvider from './contexts/AutocompleteProvider';

const App = () => {
  return (
    <AutocompleteProvider>
<span>a</span>
      <CharacterAutocomplete />
    </AutocompleteProvider>
  );
}

export default App;
